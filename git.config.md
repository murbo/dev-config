## Use Intellij as git diff and merge tool

```
git config --global difftool.prompt false
git config --global mergetool.prompt false
```

- Add the following to "~/.gitconfig" file

# MacOS

```
[merge]
    tool = intellij
[mergetool "intellij"]
    cmd = idea merge \"$LOCAL\" \"$REMOTE\" \"$BASE\" \"$MERGED\"
    trustExitCode = true
[diff]
    tool = intellij
[difftool "intellij"]
    cmd = idea diff \"$LOCAL\" \"$REMOTE\"
[difftool]
    prompt = false
```

# Windows

- Set PATH variable:

```
C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2018.1.4\bin
```

- Update git global config

```
[merge]
    tool = intellij
[mergetool]
    prompt = false
[mergetool "intellij"]
    cmd = cmd.exe //c "idea.bat  merge \"$LOCAL\" \"$REMOTE\" \"$BASE\" \"$MERGED\""
    trustExitCode = true
[diff]
    tool = intellij
[difftool]
    prompt = false
[difftool "intellij"]
    cmd = cmd.exe //c "idea.bat diff \"$LOCAL\" \"$REMOTE\""
```

If getting warning "WARNING: Could not open/create prefs root node..." here you can find a fix: https://stackoverflow.com/a/17552837
